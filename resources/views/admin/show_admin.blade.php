@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="card-title"><i class="fas fa-sitemap"></i> Nama Admin : {{$adminTenant->full_name}}</h4>
        </div>

        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <a class="btn btn-info d-none d-lg-block m-l-15"
                href="/admin/edit/{{$adminTenant->email}}"><i class="fa fa-plus-circle"></i> Ubah </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header bg-success">
                    <h4 class="m-b-0 text-white">Info Admin</h4>
                </div>

                <div class="class-body">

                    <div class="mx-5">
                        <div class="row mt-3">

                            <div class="col-md-6">
                                @include('admin.ui_show_admin', [
                                    'label' => 'Nama Depan',
                                    'data' => $adminTenant->first_name
                                ])

                                @include('admin.ui_show_admin', [
                                    'label' => 'Nama Tengan',
                                    'data' => $adminTenant->middle_name ?? ''
                                ])

                                @include('admin.ui_show_admin', [
                                    'label' => 'Nama Belakang',
                                    'data' => $adminTenant->last_name
                                ])

                                @include('admin.ui_show_admin', [
                                    'label' => 'Username',
                                    'data' => $adminTenant->username
                                ])

                                @include('admin.ui_show_admin', [
                                    'label' => 'Email',
                                    'data' => $adminTenant->email
                                ])

                                @include('admin.ui_show_admin', [
                                    'label' => 'Password',
                                    'data' => "**********"
                                ])
                            </div>

                            <div class="col-md-6">
                                @include('admin.ui_show_admin', [
                                    'label' => 'Tenant',
                                    'data' => $adminTenant->description_user->tenant_master->name
                                ])

                                @include('admin.ui_show_admin', [
                                    'label' => 'Tenant Kode',
                                    'data' => $adminTenant->description_user->tenant_master->code
                                ])

                                @include('admin.ui_show_admin', [
                                    'label' => 'Deskripsi Admin',
                                    'data' => $adminTenant->description_user->description
                                ])

                                @include('admin.ui_show_admin', [
                                    'label' => 'Status',
                                    'data' => $adminTenant->description_user->is_active ? 'Admin Aktif' : 'Tidak aktif'
                                ])
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection

