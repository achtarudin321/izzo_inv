@extends('layout.app')
@section('css')

@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">List Karyawan/Pengguna</h4>
        </div>

        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <a class="btn btn-info d-none d-lg-block m-l-15"
                href="/employee/create"><i class="fa fa-plus-circle"></i> Karyawan/Pengguna Baru</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    {{-- Alert success--}}
                    @if (session('success'))
                        @include('ui.alert.alert_success', ['message' => session('success')])
                    @endif

                    {{-- Alert error--}}
                    @if (session('error'))
                        @include('ui.alert.alert_danger', ['message' => session('error')])
                    @endif

                    <h4 class="card-title">List Karyawan/Pengguna</h4>

                    <div class="table-responsive mt-5">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>Nama</th>
                                    <th>Telephone</th>
                                    <th>Alamat</th>
                                    <th>Cabang</th>
                                    <th>Role/Peran</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($userTenant as $user)
                                    @if ($user->user_master->hasAccess('user.admin.branch'))
                                        <tr>
                                            <td class="text-center">{{$user->user_id}}</td>
                                            <td>{{$user->complete_name}}</td>
                                            <td>{{$user->phone ?? '-'}}</td>
                                            <td>{{$user->address}}</td>
                                            <td>{{$user->branch->name ?? '-'}}</td>
                                            <td>
                                                @foreach ($user->user_master->roles as $role)
                                                    {{$role->name}}
                                                @endforeach
                                            </td>
                                            <td class="text-center">
                                                <a href="/employee/{{$user->user_id}}" class="btn btn-info btn-sm">
                                                    <i class="fas fa-info-circle"></i>
                                                    Lihat
                                                </a>
                                                @if ($user->user_master->description_user->is_active)
                                                    <a href="" class="btn btn-danger btn-sm">
                                                        <i class="fas fa-ban"></i> Non-Aktifkan
                                                    </a>
                                                @else
                                                    <a href="" class="btn btn-success btn-sm">
                                                        <i class="fas fa-photo"></i> Aktifkan </a>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @empty
                                    <tr>
                                        <td colspan="7" class="text-center">Belum Ada Karyawan/Pengguna</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
