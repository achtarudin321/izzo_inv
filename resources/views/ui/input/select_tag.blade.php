<div class="form-group">
    <label>{{$label}}</label>
    <select class="form-control custom-select" id="{{$name}}" name="{{$name}}">
        <option  value="0" selected>--{{$label}}--</option>
            @foreach ($options as $item)
                @if (isset($selected))
                <option value="{{$item->id}}" {{ ($selected == $item->id ? "selected":"") }}>{{$item->name}}</option>
                @else
                <option value="{{$item->id}}" {{ (old($name) == $item->id ? "selected":"") }}>{{$item->name}}</option>
                @endif
            @endforeach
    </select>
    @if ($errors->first($name))
        <small class="form-control-feedback text-danger"> {{$errors->first($name)}} </small>
    @endif
</div>
