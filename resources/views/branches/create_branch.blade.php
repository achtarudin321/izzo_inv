@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Tambah Kantor Baru</h4>
        </div>
    </div>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                        {{-- Alert success--}}
                        @if (session('success'))
                            @include('ui.alert.alert_success', ['message' => session('success')])
                        @endif

                        {{-- Alert error--}}
                        @if (session('error'))
                            @include('ui.alert.alert_danger', ['message' => session('error')])
                        @endif

                    <form action="/branches" method="POST" autocomplete="off">
                        <div class="row mt-3 mx-5">

                            @csrf

                            <div class="col-md-6">
                                {{-- Code --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Kode Cabang',
                                    'id' => 'code',
                                    'name' => 'code',
                                    'placeholder' => 'Kode Cabang',
                                ])

                                {{-- Branch--}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Cabang',
                                    'id' => 'name',
                                    'name' => 'name',
                                    'placeholder' => 'Nama Cabang'
                                ])

                                {{-- Owner name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Pemilik / Penanggung Jawab',
                                    'id' => 'owner_name',
                                    'name' => 'owner_name',
                                    'placeholder' => 'Pemilik / Penanggung Jawab'
                                ])

                                {{-- Address --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Alamat',
                                    'id' => 'address',
                                    'name' => 'address',
                                    'placeholder' => 'Alamat'
                                ])

                            </div>

                            <div class="col-md-6">
                                {{-- Telephone --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Telephone',
                                    'id' => 'phone',
                                    'name' => 'phone',
                                    'placeholder' => 'Telephone'
                                ])

                                {{-- Fax --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Fax',
                                    'id' => 'fax',
                                    'name' => 'fax',
                                    'placeholder' => 'Fax'
                                ])

                                {{-- Email --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Email',
                                    'type' => 'email',
                                    'id' => 'email',
                                    'name' => 'email',
                                    'placeholder' => 'Email'
                                ])

                                {{-- Deskripsi --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Deskripsi',
                                    'id' => 'description',
                                    'name' => 'description',
                                    'placeholder' => 'Deskripsi'
                                ])

                                <div class="float-right">
                                    <button class="btn btn-info" type="submit">Kirim</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
