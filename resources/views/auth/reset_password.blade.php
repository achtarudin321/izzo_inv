@extends('auth.layout.layout_auth')
@section('css')

@endsection

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-4">

                <div class="card rounded">
                    <div class="card-body">

                            {{-- Title --}}
                            <div class="text-center">
                                <h4 class="card-title">Silakan buat password baru </h4>
                            </div>

                        <form action="/reset-password" method="post" autocomplete="off">

                            @csrf

                            {{-- Password --}}
                            @include('auth.UI.tag_input', [
                                'label' => 'Password',
                                'placeholder' => 'Password',
                                'icon' => 'ti-lock',
                                'name' => 'password',
                                'type' => 'password'
                            ])

                            {{-- Confirm password --}}
                            @include('auth.UI.tag_input', [
                                'label' => 'Konfirmasi Password',
                                'placeholder' => 'Konfirmasi Password',
                                'icon' => 'ti-lock',
                                'name' => 'password_confirmation',
                                'type' => 'password'
                            ])

                            <div class="float-right">
                                <button type="submit" class="btn btn-success">Kirim</button>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
