<!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Document</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/assets')}}/images/favicon.png">
    <link rel="stylesheet" href="{{mix("/css/app.css")}}">
    <link href="{{url('/css/style.min.css')}}" rel="stylesheet">
    <link href="{{url('/css/font-awesome/css/fontawesome-all.css')}}" rel="stylesheet">

    @yield('css')

</head>
<html>
    <body>

        @yield('content')

    <script src="{{mix("/js/app.js")}}"></script>
    <script src="{{url('/js/popper.min.js')}}"></script>
    <script src="{{url('/js/bootstrap.min.js')}}"></script>
    <script src="{{url('/js/sticky-kit.min.js')}}"></script>
    <script src="{{url('/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{url('/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{url('/js/waves.js')}}"></script>
    <script src="{{url('/js/sidebarmenu.js')}}"></script>
    <script src="{{url('/js/custom.min.js')}}"></script>

    @yield('js')
</body>
</html>
