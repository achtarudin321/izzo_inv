<div class="form-group">
    <label for="exampleInputuname">{{$label}}</label>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">
                <i class="{{ $icon ??'ti-user'}}"></i>
            </span>
        </div>
        <input type="{{ $type ?? 'text'}}" class="form-control" placeholder="{{$placeholder}}" name="{{$name}}"
            aria-label="{{Str::slug($label)}}" aria-describedby="basic-addon1">
    </div>
    @if ($errors->first($name))
        <small class="form-control-feedback text-danger"> {{$errors->first($name)}} </small>
    @endif
</div>
