<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Lupa Password ? Silakan masukan email kamu</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="/forgot-password" method="POST" autocomplete="off">
                @csrf

                @include('ui.input.input_tag', [
                    'label' => 'Email',
                    'id' => 'email',
                    'type' => 'email',
                    'placeholder' => 'Email',
                    'name' => 'email'
                ])

                <div class="float-right">
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>

        </div>
    </div>
</div>
