@extends('auth.layout.layout_auth')

@section('css')
@endsection

@section('content')

    {{-- container --}}
    <div class="container mt-5">

        {{-- Alert ThrottlingException--}}
        @if (session('ThrottlingException'))
            @include('ui.alert.throttling', ['data' => session('ThrottlingException')])
        @endif

        {{-- Alert AccountNotFound--}}
        @if (session('AccountNotFound'))
            @include('ui.alert.account_not_found', ['data' => session('AccountNotFound')])
        @endif

        {{-- Alert error--}}
        @if (session('error'))
            @include('ui.alert.alert_danger', ['message' => session('error')])
        @endif

        {{-- Alert error--}}
        @if (session('success'))
            @include('ui.alert.alert_success', ['message' => session('success')])
        @endif

        <div class="row justify-content-center">
            <div class="col-md-4">

                <div class="card rounded">
                    <div class="card-body">
                        {{-- Title --}}
                        <div class="text-center">
                            <h4 class="card-title">Login</h4>
                            <h6 class="card-subtitle">made with bootstrap elements</h6>
                        </div>

                        {{-- Form --}}
                        <form action="/login" method="POST" autocomplete="off">

                            {{-- Crsf Token --}}
                            @csrf

                            {{-- Username --}}
                            @include('auth.UI.tag_input', [
                                'label' => 'Username',
                                'placeholder' => 'Username',
                                'icone' => 'ti-user',
                                'name' => 'username'

                            ])

                            {{-- password --}}
                            @include('auth.UI.tag_input', [
                                'label' => 'Password',
                                'placeholder' => 'Password',
                                'icon' => 'ti-lock',
                                'name' => 'password',
                                'type' => 'password'
                            ])

                            {{-- remember me --}}
                            @include('auth.UI.tag_checkbox', [
                                'label' => 'Ingat saya',
                                'name' => 'rememberMe'
                            ])

                            {{-- Button Login --}}
                            <div class="float-right">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Login</button>
                            </div>

                            {{-- Forgot Password Trigger Modal --}}
                            <a href="#" class="badge badge-danger" data-toggle="modal" data-target="#exampleModal">Lupa Password</a>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    {{-- Modal Forgot Password --}}
    @include('auth.modal_form_forgot_password')

@endsection

@section('js')
@endsection
