@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">

        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Tambah Penyewa Baru</h4>
        </div>

    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <form action="/tenant/store" method="POST" autocomplete="off">
                        <div class="row mt-2 ">

                            @csrf

                            {{-- First Col --}}
                            <div class="col-md-4">

                                {{-- Code --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Kode',
                                    'id' => 'code',
                                    'name' => 'code',
                                    'placeholder' => 'Kode',
                                ])

                                {{-- Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Penyewa',
                                    'id' => 'name',
                                    'name' => 'name',
                                    'placeholder' => 'Nama'
                                ])

                                {{-- NPWP --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'NPWP',
                                    'id' => 'npwp',
                                    'name' => 'npwp',
                                    'placeholder' => 'NPWP'
                                ])

                                {{-- Owner --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Penanggung Jawab',
                                    'id' => 'owner',
                                    'name' => 'owner',
                                    'placeholder' => 'Penanggung Jawab'
                                ])

                                {{-- Telepone --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Telephone',
                                    'id' => 'phone',
                                    'name' => 'phone',
                                    'placeholder' => 'Telephone'
                                ])

                            </div>

                            <div class="col-md-4">

                                {{-- Fax --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Fax',
                                    'id' => 'fax',
                                    'name' => 'fax',
                                    'placeholder' => 'Fax'
                                ])

                                {{-- Email --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Email',
                                    'id' => 'email',
                                    'name' => 'email',
                                    'placeholder' => 'Email'
                                ])

                                {{-- Address --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Alamat',
                                    'id' => 'address',
                                    'name' => 'address',
                                    'placeholder' => 'Alamat'
                                ])

                                {{-- Deskripsi --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Deskripsi',
                                    'id' => 'description',
                                    'name' => 'description',
                                    'placeholder' => 'Deskripsi'
                                ])
                            </div>

                            {{-- Seconds Col --}}
                            <div class="col-md-4">

                                {{-- Host --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Host',
                                    'id' => 'host',
                                    'name' => 'host',
                                    'placeholder' => 'Host'
                                ])

                                {{-- Port --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Port',
                                    'id' => 'port',
                                    'name' => 'port',
                                    'placeholder' => 'Port'
                                ])

                                {{-- Username --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Username',
                                    'id' => 'username',
                                    'name' => 'username',
                                    'placeholder' => 'Username'
                                ])

                                {{-- Nama Database --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Database',
                                    'id' => 'db_name',
                                    'name' => 'db_name',
                                    'placeholder' => 'Nama Database'
                                ])

                                {{-- Password Database --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Password Database',
                                    'id' => 'pass',
                                    'name' => 'pass',
                                    'placeholder' => 'Password Database'
                                ])
                            </div>
                        </div>

                        <div class="float-right">
                            <button class="btn btn-info" type="submit">Kirim</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection
