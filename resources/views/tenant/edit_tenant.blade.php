@extends('layout.app')

@section('css')

@endsection

@section('content')
    {{$tenant}}

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="card-title">
                <i class="fas fa-sitemap"></i> Edit Tenant : {{$tenant->name}}
            </h4>
        </div>
    </div>


    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">Tenant : {{$tenant->name}}</h4>
                </div>

                {{-- Alert success--}}
                @if (session('success'))
                    @include('ui.alert.alert_success', ['message' => session('success')])
                @endif

                {{-- Alert error--}}
                @if (session('error'))
                    @include('ui.alert.alert_danger', ['message' => session('error')])
                @endif

                <div class="card-body">
                    <form action="/tenant/{{$tenant->id}}/update" method="post">
                        <div class="row mt-3 mx-5">
                            @csrf
                            @method("PUT")
                            <div class="col-md-6">
                                {{-- Code --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Kode Tenant',
                                    'id' => 'code',
                                    'name' => 'code',
                                    'value' => $tenant->code,
                                    'readonly' => 'readonly',
                                    'placeholder' => 'Kode Cabang',
                                ])

                                {{-- Branch--}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Tenant',
                                    'id' => 'name',
                                    'name' => 'name',
                                    'value' => $tenant->name,
                                    'placeholder' => 'Nama Cabang'
                                ])

                                {{-- Npwp--}}
                                @include('ui.input.input_tag', [
                                    'label' => 'NPWP',
                                    'id' => 'npwp',
                                    'name' => 'npwp',
                                    'value' => $tenant->npwp,
                                    'placeholder' => 'Nama Cabang'
                                ])

                                {{-- Owner--}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Pemilik / Penanggung Jawab',
                                    'id' => 'owner',
                                    'name' => 'owner',
                                    'value' => $tenant->owner,
                                    'placeholder' => 'Nama Cabang'
                                ])

                                {{-- Deskripsi --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Deskripsi',
                                    'id' => 'description',
                                    'name' => 'description',
                                    'value' => $tenant->description,
                                    'placeholder' => 'Deskripsi'
                                ])
                            </div>

                            <div class="col-md-6">
                                {{-- Telephone --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Telephone',
                                    'id' => 'phone',
                                    'name' => 'phone',
                                    'value' => $tenant->phone,
                                    'placeholder' => 'Telephone'
                                ])

                                {{-- Fax --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Fax',
                                    'id' => 'fax',
                                    'name' => 'fax',
                                    'value' => $tenant->fax,
                                    'placeholder' => 'Fax'
                                ])

                                {{-- Email --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Email',
                                    'type' => 'email',
                                    'id' => 'email',
                                    'name' => 'email',
                                    'value' => $tenant->email,
                                    'readonly' => '',
                                    'placeholder' => 'Email'
                                ])

                                {{-- Address --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Alamat',
                                    'id' => 'address',
                                    'name' => 'address',
                                    'value' => $tenant->address,
                                    'placeholder' => 'Alamat'
                                ])

                                <div class="float-right mt-5">
                                    <button class="btn btn-info" type="submit">Kirim</button>
                                </div>
                            </div>

                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
