<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Master\TenantMaster\TenantMasterModel;
use App\Repository\ConnectionRepository\ConnectionRepositoryInterface;

class MigrateTenant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:tenant {--tenant_code=} {--new} {--rollback} {--seed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrate Tenant's Database";

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $connect;

    public function __construct(ConnectionRepositoryInterface $connect)
    {
        $this->connect = $connect;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if (!$this->option('tenant_code')) {
            $this->error('please fill --tenant_code={tenant_code}');
            die;
        }

        $tenant = TenantMasterModel::where('code', $this->option('tenant_code'))->first();

        if (!$tenant) {
            $this->error('Tenant Code "' . $this->option('tenant_code') . '" Not Found!');
            die;
        }

        if($this->option('seed')) {
            $this->seederToTenant($tenant);
        }

        if ($this->option('rollback')) {

            $this->rollbackDatabase($tenant);

        }

        if ($this->option('new')) {

            $this->migrationDBTenant($tenant);
        }


    }

    protected function migrationDBTenant($tenant){
        DB::statement("CREATE DATABASE IF NOT EXISTS {$tenant->db_name}");
        $this->connect->connectToTenantDatabase($tenant);
        $this->call('migrate', [
            '--database' => 'mysql_tenant',
            '--path' => '/database/migrations/tenant'
        ]);
    }

    protected function rollbackDatabase($tenant) {
        $this->connect->connectToTenantDatabase($tenant);
        $this->call('migrate:rollback', [
            '--database' => 'mysql_tenant',
            '--path' => '/database/migrations/tenant'
        ]);
        $this->info('Tenant database successfully rollbacked!');
    }

    protected function seederToTenant($tenant) {
        $this->connect->connectToTenantDatabase($tenant);
        $this->call('db:seed');
    }
}
