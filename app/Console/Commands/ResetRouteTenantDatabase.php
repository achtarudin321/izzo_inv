<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Models\Master\TenantMaster\TenantMasterModel;
use App\Repository\ConnectionRepository\ConnectionRepositoryInterface;

class ResetRouteTenantDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:menu-tenant {--tenant_code=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Menu Tenant Database / Truncate Menu Tenant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $connect;

    public function __construct(ConnectionRepositoryInterface $connect)
    {
        $this->connect = $connect;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->option('tenant_code')) {
            $this->error('please fill --tenant_code={tenant_code}');
            die;
        }

        $tenant = TenantMasterModel::where('code', $this->option('tenant_code'))->first();

        $this->seederMenuTenant($tenant);
    }

    protected function seederMenuTenant(TenantMasterModel $tenant) {
        $this->connect->connectToTenantDatabase($tenant);
        DB::connection('mysql_tenant')->statement('SET FOREIGN_KEY_CHECKS=0');
        DB::connection('mysql_tenant')->table('tenant_menu_child')->truncate();
        DB::connection('mysql_tenant')->table('tenant_menu')->truncate();
        DB::connection('mysql_tenant')->statement('SET FOREIGN_KEY_CHECKS=1');
        $this->call('db:seed');
    }
}
