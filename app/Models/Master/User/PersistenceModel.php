<?php

namespace App\Models\Master\User;

use Cartalyst\Sentinel\Persistences\EloquentPersistence;

class PersistenceModel extends EloquentPersistence
{
    protected $connection = 'mysql_master';

    protected $table = 'ms_persistences';

    protected $userModel = 'App\Models\Master\User\UserMasterModel';

    public function user()
    {
        return $this->belongsTo(static::$usersModel);
    }
}
