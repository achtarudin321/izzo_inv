<?php

namespace App\Models\Master\User;

use Cartalyst\Sentinel\Users\EloquentUser;
use App\Models\Tenant\UserTenant\UserTenantModel;
use Sentinel as Auth;

class UserMasterModel extends EloquentUser
{
    protected $connection = 'mysql_master';

    protected $table = 'ms_users';

    protected $fillable = [
        'username',
        'email',
        'password',
        'first_name',
        'middle_name',
        'last_name',
        'permissions',
        'last_login',
        'api_token'
    ];

    protected $loginNames = ['email', 'username'];

    protected static $rolesModel = 'App\Models\Master\User\RoleModel';

    protected static $persistencesModel = 'App\Models\Master\User\PersistenceModel';

    protected static $activationsModel = 'App\Models\Master\User\ActivationModel';

    protected static $remindersModel = 'App\Models\Master\User\ReminderModel';

    protected static $throttlingModel = 'App\Models\Master\User\ThrottleModel';

    protected static $descriptionUserModel = 'App\Models\Master\DescriptionUser\DescriptionUserModel';

    protected static $userTenant  = 'App\Models\Tenant\UserTenant\UserTenantModel';

    public function roles()
    {
        return $this->belongsToMany(static::$rolesModel, 'ms_role_users', 'user_id', 'role_id')->withTimestamps();
    }

    public function persistences()
    {
        return $this->hasMany(static::$persistencesModel, 'user_id');
    }

    public function activations()
    {
        return $this->hasMany(static::$activationsModel, 'user_id');
    }

    public function reminders()
    {
        return $this->hasMany(static::$remindersModel, 'user_id');
    }

    public function throttle()
    {
        return $this->hasMany(static::$throttlingModel, 'user_id');
    }

    public function description_user()
    {
        return $this->hasOne(static::$descriptionUserModel, 'user_id')
        ->withDefault([
            'description' => 'User Master'
        ]);
    }

    public function user_tenant()
    {
        return $this->hasOne(UserTenantModel::class, 'user_id', 'id');
    }

    public function getFullNameAttribute() {
        return "{$this->first_name} {$this->middle_name} {$this->last_name}";
    }

    public function getTenantIdAttribute() {

        return Auth::getUser()->description_user->tenant_id ?? '';
    }

    public function createRole() {
        return Auth::getRoleRepository()->createModel();
    }
}
