<?php

namespace App\Models\Master\User;

use Cartalyst\Sentinel\Reminders\EloquentReminder;

class ReminderModel extends EloquentReminder
{
    protected $connection = 'mysql_master';

    protected $table = 'ms_reminders';

    protected $fillable = [
        'code',
        'completed',
        'completed_at',
    ];

}
