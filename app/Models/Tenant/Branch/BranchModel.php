<?php

namespace App\Models\Tenant\Branch;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Tenant\UserTenant\UserTenantModel;

class BranchModel extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql_tenant';

    protected $fillable = [
        'code',
        'name',
        'owner_name',
        'phone',
        'fax',
        'email',
        'address',
        'description',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $table = 'tn_branch';

    public function user_tenant()
    {
        return $this->hasOne( UserTenantModel::class, 'branch_id');
    }

}
