<?php

namespace App\Models\Tenant\Menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuTenantModel extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql_tenant';

    protected $table = 'tenant_menu';

    protected $fillable = [
        'code', 'name', 'url', 'icon', 'sort', 'type', 'description', 'is_active', 'created_by', 'updated_by'
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function menu_children()
    {
        return $this->hasMany(ChildMenuTenantModel::class, 'menu_tenant_id', 'id');
    }


}
