<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPasswordMail extends Mailable
{
    use Queueable, SerializesModels;



    protected $user;
    protected $remiderCode;

    public function __construct($user, $remiderCode)
    {
        $this->user = $user;
        $this->remiderCode = $remiderCode;
    }


    public function build()
    {
        return $this->view('emails.forgot_password')
        ->subject('Reset Password')
        ->with([
            'user' => $this->user,
            'code' => $this->remiderCode->code,
        ]);
    }
}
