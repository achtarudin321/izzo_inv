<?php
namespace App\Services;
use Reminder;
use Sentinel as Auth;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\Master\User\UserMasterModel;

class PasswordUserService
{
    public function sendRemiderCode($email) {

        if(!$email){
            return redirect()->back();
        }

        $user = UserMasterModel::where('email', '=', $email)->first();

        if(!$user) {
            return redirect()->back();
        }

        $reminder = Reminder::exists($user) ?: Reminder::create($user);

        Mail::to($user->email)->send(new ForgotPasswordMail($user, $reminder));

        return redirect()->back()->with('success', 'Link untuk mengubah password telah dikirim');
    }

    public function resetPassword($email, $remaiderCode) {

        $user = UserMasterModel::where('email', '=', $email)->first();

        if(!$user) {
            return abort(404);
        }

        $reminder = Reminder::exists($user);

        if(!$reminder){
            return abort(404);
        }
        return $user->id;
    }

    public function saveNewPassword($id, $newPassword) {
        $user = UserMasterModel::find($id);

        $reminder = Reminder::exists($user);

        if(!$reminder){
            abort(404);
        }

        Reminder::complete($user, $reminder->code, $newPassword['password'] );
        return redirect('/login')->with(['success' => "Password Anda berhasil diganti"]);
    }

    public function changePassword($oldPassword, $newPassword) {

        $user = Auth::getUser();

        if(Hash::check($oldPassword, $user->password)) {

            Auth::getUser()->update([
                'password' => Hash::make($newPassword)
            ]);
            return redirect()->back()->with('success', 'Password Berhasil diganti');
        }
        else {
            return redirect()->back()->with('error', 'Password lama anda tidak sesuai');
        }
    }
}
