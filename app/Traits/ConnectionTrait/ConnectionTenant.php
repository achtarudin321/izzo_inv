<?php
namespace App\Traits\ConnectionTrait;

trait ConnectionTenant {

    public function getConnection () {
        return $this->connection = 'mysql_tenant';
    }
}
