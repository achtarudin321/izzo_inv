<?php
namespace App\Repository\TenantRepository;

 interface TenantRepositoryInterface {

     public function createTenant (array $request);

     public function updateTenant(array $dataTenant, $idTenant);


 }
