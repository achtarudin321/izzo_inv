<?php
namespace App\Repository\TenantRepository;

use Sentinel as Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Master\TenantMaster\TenantMasterModel;

class TenantRepository implements TenantRepositoryInterface {

    public function createTenant (array $request){
        DB::beginTransaction();
        try{
            $result = TenantMasterModel::create([
                "code" =>  $request['code'],
                "name" =>  $request['name'],
                "npwp" =>  $request['npwp'],
                "owner" =>  $request['owner'],
                "address" =>  $request['address'],
                "phone" =>  $request['phone'],
                "fax" =>  $request['fax'],
                "email" =>  $request['email'],
                "description" => $request['description'],
                "db_host" =>  $request['host'],
                "db_port" =>  $request['port'],
                "db_name" => $request['db_name'],
                "db_username" => $request['username'],
                'db_password' => $request['pass'] ?? '',
                'created_by' => Auth::getUser()->id
            ]);
            DB::commit();
            return redirect('/tenant')->with('success', 'Tenant berhasil di buat');
        }
        catch(\Exception $e) {
            DB::rollback();
            return  redirect('/tenant')->with('error', $e->getMessage());
        }
    }

    public function updateTenant(array $dataTenant, $idTenant) {
        $tenant = TenantMasterModel::findOrFail($idTenant);
        $tenant->update([
            'npwp' => $dataTenant['npwp'],
            'name' => $dataTenant['name'],
            'owner' => $dataTenant['owner'],
            'phone' => $dataTenant['phone'],
            'fax' => $dataTenant['fax'],
            'email' => $dataTenant['email'],
            'address' => $dataTenant['address'],
            'description' => $dataTenant['description'],
            'updated_by' => Auth::getUser()->id,
        ]);
        return redirect("/tenant/{$idTenant}")->with('success', 'Tenant berhasil di Update');
    }
}
