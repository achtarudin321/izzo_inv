<?php
namespace App\Repository\BranchRepository;

use Sentinel as Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant\Branch\BranchModel;

class BranchRepository {

    public function getAllBranch() {
        return BranchModel::paginate(5);
    }

    public function findBranch($param) {
        return BranchModel::where('email', '=', $param)->firstOrFail();
    }

    public function createBranch(array $newBranch) {
        $newBranch['created_by'] = Auth::getUser()->id;
        $newBranch['updated_by'] = null;
        DB::beginTransaction();
        try{
            BranchModel::create($newBranch);
            DB::commit();
            return redirect('/branches')->with('success', "Berhasil menambahkan kantor cabang {$newBranch['name']}");
        }
        catch(\Exception $e) {
            DB::rollback();
            return redirect('/branches/create')->with('error', $e->getMessage());
        }
    }

    public function editBranch($param) {
        return BranchModel::where('email', '=', $param)->firstOrFail();

    }

    public function updateBranch($param, $email) {
        $branch  = BranchModel::where('email', '=', $email)->firstOrFail();
        $branch->update($param);
        return redirect("/branches/{$branch->email}")->with('success', "Kantor {$branch->name} di telah di update");
    }

    public function deleteBranch($param) {

    }

    public function enable($params) {
        $branch  = BranchModel::where('email', '=', $params)->firstOrFail();
        $branch->update(['status' => true]);
        return redirect()->back()->with('success', "Kantor {$branch->name} di telah di Aktifkan");
    }

    public function disable($params) {
        $branch  = BranchModel::where('email', '=', $params)->firstOrFail();
        $branch->update(['status' => false]);
        return redirect()->back()->with('error', "Kantor {$branch->name} di telah di Non-Aktifkan");
    }
}
