<?
namespace App\Repository\BranchRepository;

use App\Models\Tenant\Branch\BranchModel;

interface BranchRepositoryInterface {

    public function getAllBranch(BranchModel $branch);

    public function findBranch($param);

    public function createBranch(array $newBranch);

    public function editBranch($param);

    public function deleteBranch($param);
}
