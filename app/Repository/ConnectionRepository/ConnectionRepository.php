<?php
namespace App\Repository\ConnectionRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Models\Master\TenantMaster\TenantMasterModel;

class ConnectionRepository implements ConnectionRepositoryInterface
{
    public function connectToTenantDatabase (TenantMasterModel $tenant) {
        DB::purge('mysql_tenant');

        config([
            'database.connections.mysql_tenant' => [
                'driver' => 'mysql',
                'host' =>  $tenant->db_host,
                'port' => $tenant->db_port,
                'database' => $tenant->db_name,
                'username' => $tenant->db_username,
                'password' => $tenant->db_password,
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ]
        ]);
        DB::reconnect('mysql_tenant');
        return true;

    }

    public function databaseExist ($tenant_id) {

        $tenant =  TenantMasterModel::find($tenant_id);
        $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";

        $db = DB::select($query, [$tenant->db_name]);

        if(!empty($db)){
            return $this->connectToTenantDatabase($tenant);
        }
        else return false;
    }
}
