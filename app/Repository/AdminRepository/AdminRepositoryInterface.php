<?php
namespace App\Repository\AdminRepository;

interface AdminRepositoryInterface {

    public function createAdminTenant (array $credential);

}
