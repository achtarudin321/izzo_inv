<?php
namespace App\Repository\LoginRepository;

interface LoginRepositoryInterface {
    public function onlyLogin(array $login);
    public function loginAndRemember(array $login);
}
