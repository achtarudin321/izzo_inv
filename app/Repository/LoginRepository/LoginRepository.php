<?php
namespace App\Repository\LoginRepository;
use Sentinel as Auth;
use App\Repository\LoginRepository\LoginRepositoryInterface;

class LoginRepository implements LoginRepositoryInterface
{

    public function onlyLogin(array $login){
        Auth::authenticate($login);
        return Auth::check();
    }

    public function loginAndRemember(array $login){
        Auth::authenticateAndRemember($login);
        return Auth::check();
    }
}

