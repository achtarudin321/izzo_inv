<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;
use Sentinel as Auth;

class UpdateRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser()->hasAccess(['user.tenant', 'user.admin.tenant']);;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roleName' => 'required|unique:ms_roles,name,'.$this->segment(2),
        ];
    }

    public function messages() {
        return [
            'roleName.required' => 'Silakan isi nama role',
            'roleName.unique' => 'Nama role sudah digunakan'
        ];
    }
}
