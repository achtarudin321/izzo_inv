<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTenantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'npwp'  => 'required|numeric',
            'owner'  => 'required',
            'description' => 'required',
            'phone' => 'required|numeric',
            'fax' => 'required|numeric',
            'email' => 'required|regex:/^.+@.+$/i',
            'address' => 'required',
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Nama tenant silakan di isi',

            'npwp.required' => 'Npwp harap di isi',
            'npwp.numeric' => 'Format Npwp salah',

            'owner.required' => 'Penanggung Jawab harap di isi',
            'description.required' => 'Deskripsi harus di isi',

            'phone.required' => 'Telephone harap di isi ',
            'phone.numeric' => 'Format telephone salah',

            'fax.required' => 'Fax silakan diisi',
            'fax.numeric' => 'Format fax salah',

            'email.required' => 'Email silakan di isi',
            'email.regex' => 'Format email salah',

            'address.required' => 'Alamat silakan di isi',
        ];
    }
}
