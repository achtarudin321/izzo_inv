<?php

namespace App\Http\Requests\Password;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'new_password' => 'required|confirmed',
            'new_password_confirmation' => 'required',
        ];
    }

    public function messages() {
        return [
            'old_password.required' => 'Silakan isi password lama kamu',

            'new_password.required' => 'Silakan isi password baru kamu',
            'new_password.confirmed' => 'Password baru tidak sama',

            'new_password_confirmation.required' => 'Silakan isi password baru kamu',
        ];
    }
}
