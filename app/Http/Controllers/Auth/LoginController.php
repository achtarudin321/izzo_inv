<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostLoginRequest;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\Repository\LoginRepository\LoginRepositoryInterface;

class LoginController extends Controller
{

    protected $homeTenant = '/beranda';
    protected $homeMaster = '/dashboard';
    protected $loginPage = '/login';

    public function index () {
        return view('auth.login');
    }

    public function postLogin (LoginRepositoryInterface $login,PostLoginRequest $request) {

        $credentials = $request->validated();
        try{

            if($request->rememberMe) {

                $user = $login->loginAndRemember($credentials);

                if(!$user){
                    return redirect($this->loginPage)
                    ->with('AccountNotFound', 'Akun Tidak Tersedia');
                }

                else {
                    return $this->redirectTo($user);
                }

            }
            else {

                $user = $login->onlyLogin($credentials);

                if(!$user){

                    return redirect($this->loginPage)
                    ->with('AccountNotFound', 'Akun Tidak Tersedia');
                }

                else {
                    return $this->redirectTo($user);
                }

            }
        }

        catch(ThrottlingException $e) {

            return redirect($this->loginPage)
            ->with('ThrottlingException', $e->getDelay());

        }

        catch(NotActivatedException $e) {

            return redirect($this->loginPage)
            ->with('NotActivatedException', $e->getMessage());

        }
    }

    protected function redirectTo($result){

        if($result->hasAccess('user.master')){
            return redirect($this->homeMaster);
        }

        elseif($result->hasAccess('user.tenant')){
            return redirect($this->homeTenant);
        }
    }
}
