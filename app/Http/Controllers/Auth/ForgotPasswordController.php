<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PasswordUserService;

class ForgotPasswordController extends Controller
{
    protected $passwordUserService;

    public function __construct(PasswordUserService $passwordUserService) {
        $this->passwordUserService = $passwordUserService;
    }

    public function forgotPassword(Request $request) {
        return $this->passwordUserService->sendRemiderCode($request->email);
    }
}
