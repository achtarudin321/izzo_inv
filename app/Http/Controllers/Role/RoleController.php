<?php

namespace App\Http\Controllers\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Menu\MenuTenantModel;
use App\Http\Requests\Role\StoreRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Repository\RoleRepository\RoleRepository;

class RoleController extends Controller
{

    protected $roleRepo;

    public function __construct(RoleRepository $roleRepo) {

        $this->roleRepo = $roleRepo;

    }
    public function index()
    {
        $allRoles = $this->roleRepo->getTenantRoles();
        return view('role.index_role', compact('allRoles'));
    }


    public function create()
    {
        $menus = MenuTenantModel::whereIn('type', ['tenant', 'general'])->get();
        return view('role.create_role', compact('menus'));
    }

    public function store(StoreRoleRequest $request)
    {
        $valid = $request->validated();

        if($valid) {
            return $this->roleRepo->createNewRole($request->all());
        }
    }

    public function show($id){
    }


    public function edit($id)
    {
        $menus = MenuTenantModel::whereIn('type', ['tenant'])->get();
        $tenantRole = $this->roleRepo->getRoleById($id);
        return view('role.edit_role', compact('tenantRole', 'menus'));
    }


    public function update(UpdateRoleRequest $request, $id)
    {
        $valid = $request->validated();

        if($valid) {
            return $this->roleRepo->updateRole($id, $request->all());
        }
    }


    public function destroy($id)
    {
        //
    }
}
