<?php

namespace App\Http\Middleware\AuthMiddleware;

use Closure;
use Sentinel as Auth;

class NonTenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()->hasAccess('user.master')){
            return $next($request);
        }
        else {
            return abort(404);
        }
    }
}
