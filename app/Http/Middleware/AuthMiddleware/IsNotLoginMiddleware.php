<?php

namespace App\Http\Middleware\AuthMiddleware;

use Closure;
use Sentinel as Auth;

class IsNotLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($result = !Auth::check()){
            return $next($request);
        }
        else {
            return redirect('/');
        }
    }
}
