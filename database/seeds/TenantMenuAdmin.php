<?php

use Illuminate\Database\Seeder;
use App\Models\Tenant\Menu\MenuTenantModel;

class TenantMenuAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminMenu = MenuTenantModel::create([
            'code' => 'admin',
            'name' => 'Admin',
            'url' => '',
            'icon' => '',
            'sort' => '2',
            'description' => 'Parent Admin',
            'type' => 'tenant'
        ]);

        $adminMenu->menu_children()->create([
            'code' => 'branches',
            'name' => 'Cabang',
            'url' => '/branches',
            'sort' => 1,
            'description' => 'Child page branches',
            'is_active' => true,
        ]);

        $adminMenu->menu_children()->create([
            'code' => 'employee',
            'name' => 'Karyawan/Pengguna',
            'url' => '/employee',
            'sort' => 2,
            'description' => 'Child page branches',
            'is_active' => true,
        ]);

        $adminMenu->menu_children()->create([
            'code' => 'roles',
            'name' => 'Roles',
            'url' => '/roles',
            'sort' => 3,
            'description' => 'Child page branches',
            'is_active' => true,
        ]);
    }
}
