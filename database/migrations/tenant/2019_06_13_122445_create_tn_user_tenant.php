<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTnUserTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_tenant')->dropIfExists('tn_tenant_user');
        Schema::connection('mysql_tenant')->create('tn_user_tenant', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->string('complete_name');
            $table->text('address')->nullable()->default(null);
            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('inv_master.ms_users');
            $table->foreign('created_by')->references('id')->on('inv_master.ms_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_tenant')
        ->dropIfExists('tn_user_tenant');
    }
}
