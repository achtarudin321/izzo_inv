<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToTableTnUserTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tn_user_tenant', function (Blueprint $table) {
            $table->string('gender')->nullable()->default('')->after('address');
            $table->string('phone')->nullable()->default('')->after('gender');
            $table->string('path_img')->nullable()->default('')->after('phone');
            $table->integer('branch_id')->unsigned()->nullable()->after('path_img');
            $table->integer('updated_by')->unsigned()->nullable()->after('created_at');

            $table->foreign('branch_id')->references('id')->on('tn_branch');
            $table->foreign('updated_by')->references('id')->on('inv_master.ms_users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tn_user_tenant', function (Blueprint $table) {
            $table->dropForeign(['branch_id']);
            $table->dropForeign(['updated_by']);
            $table->dropColumn('gender');
            $table->dropColumn('phone');
            $table->dropColumn('path_img');
            $table->dropColumn('branch_id');
            $table->dropColumn('updated_by');
        });
    }
}
