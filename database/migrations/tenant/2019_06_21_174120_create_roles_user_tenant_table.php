<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesUserTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('roles_user_tenant');
        Schema::create('roles_user_tenant', function (Blueprint $table) {

            $table->integer('user_tenant_id')->unsigned();
            $table->integer('roles_id')->unsigned();
            $table->nullableTimestamps();
            $table->engine = 'InnoDB';
            $table->primary(['roles_id', 'user_tenant_id']);
            $table->foreign('roles_id')->references('id')->on('roles');
            $table->foreign('user_tenant_id')->references('user_id')->on('tn_user_tenant');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles_user_tenant');
    }
}
